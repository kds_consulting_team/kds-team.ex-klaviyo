## Parameters

1. API Token
    - Your Klaviyo Account API Token
    - To Fetch your API Token:
      1. Log into your Klaviyo platform
      2. Your profile name (top right)
      3. Account
      4. Settings
      5. API Keys
      6. Create API Key (if required)

2. Endpoints
    1. [Campaigns](https://www.klaviyo.com/docs/api/campaigns#campaigns)
        - Summary information for all campaigns you've created that includes the name, ID, list, subject, from email address, from name, status, and date created
    2. [Campaign Recipients](https://www.klaviyo.com/docs/api/campaigns#campaign-recipients)
        - Summary information about email recipients for the campaign specified that includes each recipients email, customer ID, and status
        - Since this endpoint requires the result from **Campaigns**, the component will automatically download content from **Campaigns** even if **Campaigns** is not selected
    3. [Lists](https://www.klaviyo.com/docs/api/v2/lists)
        - Get a listing of all of the lists in an account
    4. [Group Members](https://www.klaviyo.com/docs/api/v2/lists#get-members-all)
        - Get all of the emials, phone numbers for profiles from lists available
        - Since this endpoint requires the result from **Lists**, the component will automatically download content from **Lists** even if **Lists** is not selected
    5. [Email Templates](https://www.klaviyo.com/docs/api/email-templates#email-templates)
        - Returns a list of all the email templates you've created
    6. [Metrics](https://www.klaviyo.com/docs/api/metrics#metrics)
        - Returns a list of all the metrics
    7. [Metrics Timeline](https://www.klaviyo.com/docs/api/metrics#metric-timeline)
        - Returns a batched timeline of all events in your Klaviyo account
        - The component will store the latest metric event offset and it will be used for the next run.
        - The component has been restricted to fetch 10,000 metric timeline records due to timeout limits. If your account contains more Metrics Events, please run this endpoint more frequently.
    8. [Person Details by Segment](https://www.klaviyo.com/docs/api/v2/lists#get-members)
        - Fetching all the data attributes for a person from the input segment
        - If you want to fetch all person profiles, please refer to `How-to` in **Segments** below

3. Metric Timeline Start Date
    - Required when **Metrics Timeline** is selected
    - This will inform the component on when the component will start fetching the metric timeline. An updated metric event offset will be stored in the state of the configuration for next job run.

4. Segments
    - Required when **Person Details by Segment** is selected
    - The segment which the component will be used to fetch the list of person details
    - According to Klaivyo, you can create the segment below to fetch all person profiles:
        - Segment definitions:
            - Condition: `Properties about someone`
            - Dimension: `Email`
            - Dimension Value: `is set` OR `is not set`
        - Please refer to this [screenshot](https://bitbucket.org/kds_consulting_team/kds-team.ex-klaviyo/src/master/docs/ALL_CUSTOMER_SEGMENT.png) if you are unsure
          