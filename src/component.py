'''
Template Component main class.

'''

import csv
import datetime  # noqa
import datetime  # noqa
import json
import logging
import os  # noqa
import re
import sys
import time

import backoff
import pandas as pd
import requests  # noqa
from kbc.env_handler import KBCEnvHandler
from kbc.result import KBCTableDef  # noqa
from kbc.result import ResultWriter  # noqa

from requests.exceptions import RequestException

from klaviyo import Klaviyo

# configuration variables
KEY_DEBUG = 'debug'
KEY_API_TOKEN = '#api_token'
KEY_ENDPOINT = 'endpoints'
KEY_METRIC_TIMELINE_START_DATE = 'metric_timeline_start_date'
KEY_SEGMENTS = 'segments'

MANDATORY_PARS = [
    KEY_API_TOKEN,
    KEY_ENDPOINT,
    KEY_METRIC_TIMELINE_START_DATE,
    KEY_SEGMENTS
]
MANDATORY_IMAGE_PARS = []

# Default Table Output Destination
DEFAULT_TABLE_SOURCE = "/data/in/tables/"
DEFAULT_TABLE_DESTINATION = "/data/out/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/files/"
DEFAULT_FILE_SOURCE = "/data/in/files/"

BASE_URL = 'https://a.klaviyo.com/api/'
REQUEST_MAP = {
    'campaigns': {
        'endpoint': 'v1/campaigns',
        'mapping': 'campaigns',
        'primary_key': 'id',
        'dataField': 'data'
    },
    'campaign_recipients': {
        'endpoint': 'v1/campaign/{child_id}/recipients',
        'required': 'campaigns',
        'mapping': 'recipients',
        'primary_key': 'id',
        'dataField': 'data'
    },
    'lists': {
        'endpoint': 'v2/lists',
        'mapping': 'lists',
        'primary_key': 'list_id',
        'dataField': '.'
    },
    'group_members': {
        'endpoint': 'v2/group/{child_id}/members/all',
        'required': 'lists',
        'mapping': 'group_members',
        'primary_key': 'id',
        'dataField': 'records',
        'pagination': 'marker'
    },
    'email_templates': {
        'endpoint': 'v1/email-templates',
        'mapping': 'email_templates',
        'primary_key': 'id',
        'dataField': 'data'
    },
    'metrics': {
        'endpoint': 'v1/metrics',
        'mapping': 'metrics',
        'dataField': 'data',
        'primary_key': 'id'
    },
    'metric_timeline': {
        'endpoint': 'v1/metrics/timeline?count=200',
        'mapping': 'metrics',
        'dataField': 'data',
        'primary_key': 'id'
    },
    "person_details_v2": {
        'endpoint': 'v1/person/{child_id}',
        'required': 'group_members',
        'mappping': 'person_details',
        'primary_key': 'id',
        'dataField': '.'
    }
}

ROOT_ENDPOINTS = {
    'campaigns': [],
    'lists': [],
    'email_templates': [],
    'group_members': [],
    'metrics': [],
    'metric_timeline': [],
    'person_details_v2': []
}

REQUESTED_ENDPOINTS = []
REQUEST_ORDER = [
    'campaigns',
    'campaign_recipients',
    'lists',
    'group_members',
    'email_templates',
    'metrics',
    'metric_timeline',
    'person_details_v2'
]

MISSING_HEADERS = []

APP_VERSION = '0.0.5'

now = int(datetime.datetime.now().timestamp())


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        # Disabling list of libraries you want to output in the logger
        disable_libraries = []
        for library in disable_libraries:
            logging.getLogger(library).disabled = True

        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True

        log_level = logging.DEBUG if debug else logging.INFO
        # setup GELF if available
        if os.getenv('KBC_LOGGER_ADDR', None):
            self.set_gelf_logger(log_level)
        else:
            self.set_default_logger(log_level)

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    def run(self):
        '''
        Main execution code
        '''

        params = self.cfg_params  # noqa

        # Validating if the configuration is entered properly
        self.validate_user_input(params)

        # User parameters
        self.api_token = params.get(KEY_API_TOKEN)
        endpoints = params.get(KEY_ENDPOINT)
        metric_timeline_start_date = params.get(KEY_METRIC_TIMELINE_START_DATE)
        self.segments = params.get(KEY_SEGMENTS)

        # Setting up metric timeline start date
        if metric_timeline_start_date != '':
            self.metric_timeline_timestamp = int(time.mktime(
                time.strptime(metric_timeline_start_date, "%Y-%m-%d")))
        else:
            today = datetime.date.today()
            yesterday = today - datetime.timedelta(days=1)
            self.metric_timeline_timestamp = int(time.mktime(
                time.strptime(yesterday.strftime("%Y-%m-%d"), "%Y-%m-%d")))

        # Handling state file
        if not self.get_state_file():
            self.state = {}
        else:
            self.state = self.get_state_file()

        logging.info(f'Current State: {self.state}')

        for r in REQUEST_ORDER:
            if endpoints[r]:
                self.fetch(r)

        self.write_state_file(self.state)

        logging.info("Extraction finished")

    def validate_user_input(self, params):
        '''
        Validating user inputs
        '''

        # 1 - empty configuration
        if not params:
            logging.error('Your configuration is missing.')
            sys.exit(1)

        # 2 - Validating if endpoints are selected
        endpoint_count = 0
        for endpoint in params['endpoints']:
            if params['endpoints'][endpoint]:
                endpoint_count += 1
        if endpoint_count == 0:
            logging.error('Please select at least one endpoint.')
            sys.exit(1)

        # 3 - Validating segment inputs if person-details_v2
        if params['endpoints']['person_details_v2']:
            if params['segments']['all_customer'] == '':
                logging.error(
                    'Endpoint [Person Details by Segment] requires segment ID. Segment ID is missing.')
                sys.exit(1)

        # 4 - Check credentials
        if params['#api_token'] == '':
            logging.error('Your API token is missing.')
            sys.exit(1)

        # 5 - Deprecating [Person Details]
        if params['endpoints']['person_details']:
            params['endpoints']['person_details'] = False

    def fetch(self, endpoint):
        logging.info(f'Requesting [{endpoint}]')

        request_properties = REQUEST_MAP[endpoint]

        if endpoint == 'person_details_v2':
            # validate if we had all the person_ids
            segment_id = ''
            person_ids = []

            # Fetching person_details_v2 headers from state if exist
            person_headers = self.state['person_details_v2'] if 'person_details_v2' in self.state else [
            ]

            # if 'person_details_v2' not in self.state or len(self.state['person_details_v2']) == 0:
            segment_id = self.segments['all_customer']

            logging.info(f'Fetching Segment [{segment_id}]')

            # Klaviyo SDK
            klaviyo_sdk = Klaviyo(self.api_token)

            # Fetching all person_ids
            all_profiles = klaviyo_sdk.get_profile_ids(segment_id)
            if all_profiles:
                person_ids = person_ids + all_profiles
            logging.info(f'Total person_ids: [{len(person_ids)}]')

            # Fetching all person_details
            itr_n = 1000
            lower_limit = 0  # noqa
            upper_limit = itr_n if itr_n < len(person_ids) else len(person_ids)  # noqa
            person_details_limit = len(person_ids)  # noqa

            # LOAD WITH PAGINATION
            while lower_limit <= person_details_limit:
                person_ids_array = person_ids[lower_limit:upper_limit]

                data_in, person_headers, new_person_headers = klaviyo_sdk.get_profiles_parallel(
                    person_ids_array, person_headers)

                if new_person_headers:
                    self.append_new_columns_to_table(
                        table_name=endpoint, all_output_columns=person_headers, new_output_columns=new_person_headers)

                # Outputting paginated data into output file
                self._output(df_json=data_in, filename=endpoint,
                             output_columns=person_headers)

                lower_limit += itr_n
                upper_limit += itr_n
                upper_limit = len(person_ids) if upper_limit > len(
                    person_ids) else upper_limit
                logging.info(f'Fetched [{upper_limit}] person_ids')

            logging.info(f'Total fetched Person IDs: {person_details_limit}')

            # Saving headers into state
            self.state['person_details_v2'] = person_headers

        elif 'required' in request_properties:
            # Fetch required endpoint if not requested yet
            if request_properties['required'] not in REQUESTED_ENDPOINTS:
                self.fetch(request_properties['required'])

            for itr in ROOT_ENDPOINTS[request_properties['required']]:

                endpoint_url = request_properties['endpoint'].replace(
                    '{child_id}', itr)

                pagination_loop = True
                pagination_param = {}
                group_members_itr = 0

                # Continuing pagination method for person_details
                if endpoint == 'person_details':
                    # Configure state file
                    if 'person_details' not in self.state:
                        self.state['person_details'] = []

                    # Check if we fetch the person yet
                    # and itr in self.state['person_details']:
                    if itr in ROOT_ENDPOINTS[endpoint]:
                        continue

                # Continuing pagiantion method for group_members
                if endpoint == 'group_members':
                    # state property for group_members
                    if 'group_members' not in self.state:
                        self.state['group_members'] = {}

                    # if group_members exists in state, but is empty, keboola saves an empty object as [] instead of {}
                    if not self.state["group_members"]:
                        self.state['group_members'] = {}

                    if itr in self.state['group_members']:
                        if self.state['group_members'][itr]:
                            pagination_param['marker'] = self.state['group_members'][itr]

                while pagination_loop:

                    data_in = self.get_request(
                        endpoint=endpoint_url, params=pagination_param).json()

                    # Hanlding paignation loop if pagiantion param found
                    if 'marker' in data_in:
                        pagination_param['marker'] = data_in['marker']
                    elif 'next_offset' in data_in:
                        pagination_param['offset'] = data_in['next_offset']
                    else:
                        pagination_loop = False

                    # Fetching the datafield
                    if request_properties['dataField'] == '.':
                        data_in = data_in
                    else:
                        data_in = data_in[request_properties['dataField']]

                    # making sure we are not double fetching the same person
                    if endpoint == 'person_details':
                        ROOT_ENDPOINTS[endpoint].append(data_in['id'])

                    # formatting
                    data_in = [data_in] if isinstance(data_in, dict) else data_in
                    data_out = []

                    if endpoint in ['campaign_recipients', 'group_members']:
                        for row in data_in:
                            tmp = {}
                            for item in row:
                                tmp[item] = row[item]
                            parent_key = '{}_id'.format(
                                request_properties['required'][:-1])
                            tmp[parent_key] = itr
                            data_out.append(tmp)
                            if endpoint == 'group_members':
                                ROOT_ENDPOINTS['group_members'].append(
                                    tmp['id'])

                    elif endpoint in ['person_details']:
                        logging.error(
                            'Endpoint [Person Details] is not longer supported. Please use [Person Details V2]')
                        sys.exit(1)

                    else:
                        data_out = data_in

                    # API sometimes send phonenumber in response, sometimes not, we have to have add it in cases that
                    # it is not occuring as it breaks the output table when it is not present.
                    if endpoint == 'group_members':
                        for i, datum in enumerate(data_out):
                            if "phone_number" not in datum:
                                data_out[i]["phone_number"] = ""

                    self._output(df_json=data_out, filename=endpoint)

                    # Pagination limit for group_members
                    group_members_itr += 1
                    # if len(ROOT_ENDPOINTS['group_members']) >= request_limit:
                    if group_members_itr == 5 and endpoint == 'group_members':
                        if 'marker' in pagination_param and pagination_loop:
                            self.state['group_members'][itr] = pagination_param['marker']
                        else:
                            self.state['group_members'][itr] = ''
                        break

        else:
            endpoint_url = request_properties['endpoint']

            pagination_loop = True
            pagination_param = {}
            page = 0

            # Pagination limit for metric timeline
            metric_timeline_itr = 0
            metric_timeline_limit = 100_000

            while pagination_loop:

                # Handling state
                # Check if metric_timeline has state
                if endpoint == 'metric_timeline':
                    if 'metric_timeline' in self.state:
                        pagination_param['since'] = self.state['metric_timeline']
                        pagination_param['sort'] = 'asc'
                    else:
                        pagination_param['since'] = self.metric_timeline_timestamp
                        pagination_param['sort'] = 'asc'
                    logging.info(
                        f'Pagination [since] parameters: [{pagination_param["since"]}]')

                data_in = self.get_request(
                    endpoint=endpoint_url, params=pagination_param).json()

                # Pagination
                if 'total' in data_in:
                    if data_in['end'] + 1 < data_in['total']:
                        page += 1
                        pagination_param['page'] = page
                    else:
                        pagination_loop = False

                elif 'next' in data_in and endpoint == 'metric_timeline':
                    # The end of pagination
                    if pagination_param['since'] == data_in['next'] or data_in['next'] is None:
                        pagination_loop = False

                    pagination_param['since'] = data_in['next']
                    metric_timeline_itr += 1

                    # Pagination Limit
                    if metric_timeline_itr == metric_timeline_limit:
                        logging.warning("The component has reached iteration limit of 100k for "
                                        "the metric_timeline endpoint.")
                        pagination_loop = False

                else:
                    pagination_loop = False

                # Data Output
                data_out = data_in if request_properties[
                                          'dataField'] == '.' else data_in[request_properties['dataField']]

                if not data_out:
                    logging.warning(f"No data found for endpoint : {endpoint}")
                data_out = [data_out] if isinstance(data_out, dict) else data_out

                for row in data_out:
                    ROOT_ENDPOINTS[endpoint].append(
                        row[request_properties['primary_key']])

                self._output(df_json=data_out, filename=endpoint)

                # Output pagination parameters
                tmp_json = data_in
                if 'data' in tmp_json:
                    del tmp_json['data']
                logging.info('Pagination Parameters: [{}]'.format(tmp_json))

                # Output State for metric_event
                if endpoint == 'metric_timeline' and 'next' in data_in:
                    if data_in['next'] is not None:
                        self.state[endpoint] = data_in['next']

        REQUESTED_ENDPOINTS.append(endpoint)

    @backoff.on_exception(backoff.expo, (ConnectionResetError, RequestException), max_tries=5)
    def get_request(self, endpoint, params=None, retry=0):
        if params is None:
            params = {}
        request_url = BASE_URL + endpoint
        params['api_key'] = self.api_token

        r = requests.get(url=request_url, params=params)
        if r.status_code not in [200, 201]:
            logging.error(f'Request issue: {r.text}')
            if r.status_code == 429 and retry < 10:
                logging.info(f'Retrying {retry}...')
                time.sleep(15)
                r = self.get_request(
                    endpoint=endpoint, params=params, retry=retry + 1)
            else:
                logging.error(r.json()['message'])
                sys.exit(1)

        return r

    def _output(self, df_json, filename, output_columns=None):
        if df_json:
            new_output_columns = self._convert_headers(
                output_columns) if output_columns is not None else output_columns

            # Output headers
            output_filename = f'{self.tables_out_path}/{filename}.csv'
            data_output = pd.DataFrame(df_json, dtype=str)

            # order columns because api shuffes order of attributes in responses
            data_output.sort_index(axis=1, inplace=True)

            # move id column at the beginning if exists in the df
            if 'id' in data_output.columns:
                id_column = data_output.pop('id')
                data_output.insert(0, 'id', id_column)

            if not os.path.isfile(output_filename):
                with open(output_filename, 'a') as b:
                    if output_columns is not None:
                        data_output.to_csv(
                            b, header=new_output_columns, index=False, columns=output_columns)
                    else:
                        data_output.to_csv(
                            b, index=False, columns=output_columns)
                b.close()
            else:
                with open(output_filename, 'a') as b:
                    data_output.to_csv(
                        b, index=False, header=False, columns=output_columns)
                b.close()

            self._create_manifest(filename)

    def _create_manifest(self, filename):
        # logging.info(f'Creating [{filename}] manifest')
        manifest = {
            'incremental': True
        }

        output_filename = f'{self.tables_out_path}/{filename}.csv.manifest'
        if filename in ['campaigns', 'person_details', 'metric_timeline', 'metrics', 'person_details_v2']:
            manifest['primary_key'] = ['id']
        elif filename in ['lists']:
            manifest['primary_key'] = ['list_id']
        elif filename in ['group_members']:
            manifest['primary_key'] = ['list_id', 'id']
        elif filename in ['campaign_recipients']:
            manifest['primary_key'] = ['campaign_id', 'customer_id']

        with open(output_filename, 'w') as file_out:
            json.dump(manifest, file_out)

    def _convert_headers(self, columns):
        '''
        Convert all column names to Keboola standard column name parser
        '''

        # Renaming the columns up to Keboola standards to prevent
        # any duplicate columns
        new_columns = []
        for col in columns:
            tmp_col = re.sub(r'[^0-9a-zA-Z]+', '_', col)
            tmp_col = tmp_col[:-1] if tmp_col[-1] == '_' else tmp_col
            tmp_col = tmp_col[1:] if tmp_col[0] == '_' else tmp_col

            itr = 0  # increase as same column name is found
            col_after = tmp_col

            # Rule: maximizing the number of characters allowed in a column
            if len(col_after) > 64:
                col_after = col_after[:63]

            while col_after in new_columns:
                if len(col_after) >= 63:
                    col_after = f'{tmp_col[:61]}_{itr}'
                    # col_after = f'{col_after[:61]}_{itr}'
                else:
                    col_after = f'{tmp_col}_{itr}'
                itr += 1

            '''# Rule: maximizing the number of characters allowed in a column
            if len(col_after) > 64:
                col_after = col_after[:64]'''

            new_columns.append(col_after)

        return new_columns

    def append_new_columns_to_table(self, table_name, all_output_columns, new_output_columns):
        # Creating TMP file
        output_filename = f'{self.tables_out_path}/{table_name}.csv'

        all_output_columns_converted = self._convert_headers(
            all_output_columns)
        new_output_columns_converted = self._convert_headers(
            new_output_columns)

        # Ensure the file exist
        if os.path.isfile(output_filename):
            tmp_filename = f'{self.tables_out_path}/TEMP_{table_name}.csv'
            os.system(f'mv {output_filename} {tmp_filename}')

            with open(tmp_filename, 'r') as r_csvfile:
                with open(output_filename, 'w') as w_csvfile:
                    dict_reader = csv.DictReader(r_csvfile, delimiter=',')
                    # add new column with existing
                    fieldnames = all_output_columns_converted
                    writer_csv = csv.DictWriter(
                        w_csvfile, fieldnames, delimiter=',')
                    writer_csv.writeheader()

                    for row in dict_reader:
                        for col in new_output_columns_converted:
                            row[col] = ''
                        writer_csv.writerow(row)

            r_csvfile.close()
            w_csvfile.close()

            # Removing TMP file
            os.system(f'rm {tmp_filename}')


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = True
    comp = Component(debug)
    comp.run()
