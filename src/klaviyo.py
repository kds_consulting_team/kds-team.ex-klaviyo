import time
import multiprocessing
import requests
import logging
import sys  # NOQA


class Klaviyo:

    def __init__(self, private_key):

        self.private_key = private_key

    def get_profile_ids(self, segment_id):
        '''
        get list of ids from a given list/segment
        '''

        marker = None

        ids = []
        members_url = f'https://a.klaviyo.com/api/v2/group/{segment_id}/members/all?api_key={self.private_key}'

        while True:

            if not marker:

                members_call = members_url

            else:

                members_call = f'{members_url}&marker={marker}'

            response = requests.get(members_call)

            if response.status_code == 200:

                members_content = response.json()

                ids.extend([record['id']
                            for record in members_content['records']])

                if 'marker' in members_content:

                    marker = members_content['marker']

                else:

                    break

            elif response.status_code == 429:

                sleep = eval(response.json()['detail'].split()[-2])

                logging.warning(
                    f'rate limit exceeded, # seconds to sleep: {sleep}')
                time.sleep(sleep)

            else:

                logging.error('ERROR 404')
                return None

            logging.info('IDs SAVED: [{}]'.format(len(ids)))

        logging.info('IDs SAVED: [{}]'.format(len(ids)))
        return ids

    def get_profile(self, profile_id):
        '''
        extract profile data of a given ID; if no such id, returns None
        '''

        while True:

            # logging.debug(f'Fetching Profile [{profile_id}]')
            profile_call = f'https://a.klaviyo.com/api/v1/person/{profile_id}?api_key={self.private_key}'

            response = requests.get(profile_call)

            if response.status_code == 429:

                sleep = eval(response.json()['detail'].split()[-2])

                logging.warning(
                    f'rate limit exceeded, # seconds to sleep: {sleep}')
                time.sleep(sleep)

            else:

                if response.status_code == 200:

                    profile = response.json()
                    tmp = {}

                    for header in profile:
                        output_header = header.replace('$', 'k_')
                        tmp[output_header] = profile.get(header)

                    return tmp

                else:

                    if response.status_code == 404:

                        logging.error('ERROR: 404')

                    else:

                        logging.error(
                            f'UNKNOWN RESPONSE: {response.status_code}')

                    return None

    def get_profiles_parallel(self, profile_ids, profile_headers):
        '''
        given a list of profile IDs, return a list of profiles, using parallel API calls
        '''

        cores = multiprocessing.cpu_count()
        # logging.info(f'MultiProcessing cores: {cores}')

        pool = multiprocessing.Pool(processes=cores)

        result = pool.map(self.get_profile, profile_ids)
        pool.close()
        pool.join()

        # out = [profile for profile in result if profile]

        out = []
        new_profile_headers = []

        for profile in result:
            if profile:
                out.append(profile)
                current_profile_headers = list(profile)
                new_columns = list(
                    set(current_profile_headers)-set(profile_headers))

                for col in new_columns:
                    if col not in new_profile_headers:

                        new_profile_headers.append(col)
                        profile_headers.append(col)

        return out, profile_headers, new_profile_headers
